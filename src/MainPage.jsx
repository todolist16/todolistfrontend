import { useNavigate } from "react-router";
import TasksList from "./Tasks";
import "./MainPageCover.css"

export default function MainPage(){
    const navigate = useNavigate();
    const signOut = ()=> {
        localStorage.removeItem('temitope')
        navigate('/')
      
    };

    return(
        <div style={{height: '100vh'}}>
            <div className="tasksList" 
                style={{
                    display: 'flex',  
                    justifyContent:'center', 
                    alignItems:'center'                    
                   }}>
                <TasksList/>
                <p/>
            </div>
            <button className="signOutBtn" onClick = {signOut}>Sign out</button>
        </div>
    );
}