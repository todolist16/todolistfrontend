import {Routes, BrowserRouter, Route} from 'react-router-dom';
import Login from './Forms/LoginForm';
import MainPage from './MainPage';
import { RequireToken} from './Auth'
import RegisterForm from './Forms/RegisterForm';

function App() {
    return (
        <div className ="App">
            <BrowserRouter>
                <Routes>
                    <Route path="/login" element = {<Login/>}/>
                    <Route path="/" element = {<RequireToken> <MainPage/> </RequireToken>}/>
                    <Route path = "/register" element= {<RegisterForm/>}/>
                </Routes>
            </BrowserRouter>
        </div>
    );
}

export default App;
