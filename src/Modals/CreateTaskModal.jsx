import React from 'react';
// import Button from '@mui/material/Button';
import Modal from 'react-bootstrap/Modal';
import api from "../axios";
import ClearIcon from '@mui/icons-material/Clear';
import DateTimePicker from 'react-datetime-picker';
// import "react-datetime-picker/dist/DateTimePicker.css";
import "react-calendar/dist/Calendar.css";
import "react-clock/dist/Clock.css"

export default class CreateTaskModal extends React.Component {
    constructor(props){
        super(props);
        this.state = {};
        this.state.modalShow = false;
        this.state.updateTaskList = props.updateTaskList;
        this.state.heading = "";
        this.state.description = "";
        this.state.deadlineDate = new Date();
        this.state.task = 
        this.state.errorMessage = "";
        this.taskSave = this.taskSave.bind(this);
    }

    updateHeading (value) {
        this.setState({"heading": value});
    }

    updateDescription (value) {
        this.setState({"description": value});
    }

    updateDeadline (value) {
        this.setState({"deadlineDate": value});
    }

    taskSave() {
        if(this.state.heading===''){
            this.setState({"errorMessage": "Task name field cannot be empty"});
            return
        }else{
            api.post("/task",{
                "name": this.state.heading, 
                "description": this.state.description, 
                "deadline": this.state.deadlineDate
            })
            .then(response => {
                if (response.status === 200){
                    console.log(response.data["id"]);
                    this.state.updateTaskList({
                        "name": this.state.heading, 
                        "description": this.state.description, 
                        "deadline": this.state.deadlineDate, 
                        "id": response.data["id"]
                    });
                    this.setState({"modalShow": false});
                    alert("Success");
                }
            })
            .catch(error => {
                console.log(error,'error');
            });
        }
    }


    render(){
        return (
            <>
                <button className='createButton' variant="primary" onClick={() => this.setState({"modalShow": true})}>
                  Create New Task
                </button>
                <Modal className="modalContainer" show={this.state.modalShow} onHide={() => this.setState({"modalShow": false})} task={this.state.task}>
                    <Modal.Header>
                        New Task
                    </Modal.Header>
                    <Modal.Body className='modalBody'>
                        <div>
                            <label className="modalLabel" htmlFor="text">Task name:</label>
                            <input className="taskInput" type="text" onChange={(e)=> this.updateHeading(e.target.value)} placeholder="Task name" />
                            <p/>
                            <label className="modalLabel" htmlFor="text">Deadline:</label>
                            <DateTimePicker className="DateTimePicker" onChange={(e) => this.updateDeadline(e)} />
                            <p/>
                            <label className="descrLbl" htmlFor="text">Description:</label>
                            <textarea type="text" onChange={(e)=>this.updateDescription(e.target.value)} placeholder="Task description" />
                            <p/>
                            <button className="confirmBtn" onClick={this.taskSave}>Confirm</button>
                            <div>{this.state.errorMessage}</div>
                        </div>
                        <button className="closeBtn" onClick={() =>this.setState({"modalShow": false})}>
                            <ClearIcon />
                        </button>
                    </Modal.Body>
                </Modal>
            </>
        );
    }
}