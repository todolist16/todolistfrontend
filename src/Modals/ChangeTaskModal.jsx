import React from 'react';
import Modal from 'react-bootstrap/Modal';
import api from "../axios";
import EditIcon from '@mui/icons-material/Edit';
import ClearIcon from '@mui/icons-material/Clear';
import DateTimePicker from 'react-datetime-picker';
// import "react-datetime-picker/dist/DateTimePicker.css";
// import "react-calendar/dist/Calendar.css";
// import "react-clock/dist/Clock.css";

export default class ChangeTaskModal extends React.Component {
    constructor(props){
        super(props);
        this.state = {};
        this.state.task = props.task;
        this.state.updateTask = props.updateTask;
        this.state.modalShow = false;
        this.state.heading = props.task.name;
        this.state.description = props.task.description;
        this.state.deadlineDate = props.task.deadline;
        this.state.errorMessage = "";
        this.state.taskID = props.task.id;
        this.taskSave = this.taskSave.bind(this);
    }

    updateHeading (value) {
        this.setState({"heading": value});
    }

    updateDescription (value) {
        this.setState({"description": value});
    }

    updateDeadline (value) {
        this.setState({"deadlineDate": value});
    }
  
    taskSave () {
        if(this.state.heading===''){
            this.setState({"errorMessage": "Task name field cannot be empty"});
            return
        }else{
            api.put("/task",{
                "name": this.state.heading, 
                "description": this.state.description, 
                "deadline": this.state.deadlineDate, 
                "id":this.state.taskID
            })
            .then(response => {
                console.log(response)
                if (response.status === 200){
                    this.state.updateTask({
                        "name": this.state.heading, 
                        "description": this.state.description, 
                        "deadline": this.state.deadlineDate, 
                        "id":this.state.taskID
                    })
                    this.setState({"modalShow": false});
                    alert("Success");
                }
            })
            .catch(err => {
                console.log(err,'error');
            });
        }
    }

    render(){
        return (
            <>
                <button className='specialBtns' variant="primary" onClick={() => this.setState({"modalShow": true})}>
                    <EditIcon />
                </button>
                <Modal className='modalContainer' show={this.state.modalShow} onHide={() => this.setState({"modalShow": false})} task={this.state.task}>
                    <Modal.Header>
                        Edit Task
                    </Modal.Header>
                    <Modal.Body className='modalBody'>
                        <div>
                            <form id="form">
                                <label className="modalLabel" htmlFor="text">Task name:</label>
                                <input className="taskInput" type="text" onChange={(e)=> this.updateHeading(e.target.value)} value={this.state.heading} placeholder="Task name" />
                                <p/>
                                <label className="modalLabel" htmlFor="text">Deadline:</label>
                                <DateTimePicker className="DateTimePicker" value={this.state.deadlineDate} onChange={(e) => this.updateDeadline(e)} />
                                <p/>
                                <label className="descrLbl" htmlFor="text">Description:</label>
                                <textarea type="text" onChange={(e)=>this.updateDescription(e.target.value)} value={this.state.description} placeholder="Task description" />
                                <p/>
                                <button className="confirmBtn" onClick={this.taskSave}>Confirm</button>
                                <div>{this.state.errorMessage}</div>
                            </form>
                        </div>
                        <button className="closeBtn" onClick={() =>this.setState({"modalShow": false})}>
                            <ClearIcon />
                        </button>
                    </Modal.Body>
                </Modal>                
          </>
        );
    }
}
