import { useNavigate } from "react-router";
import { fetchToken, setToken } from "../Auth";
import { useState } from "react";
import api from "../axios";
import "../FormsCover.css"

export default function Login(){

    const navigate = useNavigate();
    const [username,setUsername] = useState('');
    const [password,setPassword] = useState('');
    const [errorMessage, setErrorMessage] = useState("");
    const registration = ()=> {
        localStorage.removeItem('temitope')
        navigate('/register')
    }
  
    const loginCheck = () => {
        if(username==='' & password===''){
            setErrorMessage("Please, fill in the input fields");
            return errorMessage
        }else{
            var formData = new FormData()
            formData.append("username", username);
            formData.append("password", password);
            api.post("/login",formData)
            .then(function(response){
                console.log(response.data.access_token,'response.data.token')
                if(response.data.access_token){
                    setToken(response.data.access_token)
                    navigate("/");
                }
            })
            .catch(function(error){
                console.log(error,'error');
                setErrorMessage("Incorrect username or password. Please, try again")
            });
        }
    }

    return(
        <div style={{minHeight:940, marginTop:30}}>
            <h1 className="formsHeaders">Sign In</h1>
            <div style={{marginTop:30}} className="login">
            {
                fetchToken()
                ?(navigate("/")):(
                    <div>
                        <form className="signForm">
                            <span className="headerSpan">Sign in and enjoy the service</span>
                            <p/>
                            <label>Username:</label>
                            <input 
                            type='text' 
                            onChange={(e)=>setUsername(e.target.value)}
                            />
                            <p/>
                            <label>Password:</label>
                            <input 
                            type='password' 
                            onChange={(e)=>setPassword(e.target.value)}
                            />
                            <p/>
                            <button className="signButton" type='button' onClick = {loginCheck}>Sign In</button>
                            <p/>
                            <div>{errorMessage}</div>
                            <p/>
                            <span className="goToRegSpan"> Don't have an account?</span>
                            <button className="goToRegButton" onClick = {registration}>Sign Up</button> 
                        </form>
                    </div>
                )
            }
            </div>
            </div>  
    );
}
