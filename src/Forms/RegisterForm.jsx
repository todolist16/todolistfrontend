import React from 'react';
import { useState } from "react";
import { useForm } from 'react-hook-form';
import { useNavigate } from "react-router"
import api from '../axios';

export default function RegisterForm() {
    const navigate = useNavigate();
    const { register, handleSubmit, formState: { errors } } = useForm();
    const [errorMessage, setErrorMessage] = useState("");
    const login = ()=> {
        localStorage.removeItem('temitope')
        navigate('/login')
    }
    const onSubmit = data => {
        api.post("/registration",{username: data.username, password: data.password})
            .then(res => {
                if (res.data === "OK") {
                    alert("Success");
                    navigate("/");
                } 
            })
            .catch(function(error){
                console.log(error,'error');
                setErrorMessage('Username "' + data.username + '" already exists, please choose another one.' )
            });            
    }
    
    return (
        <section>
            <div style={{minHeight:940, marginTop:30}}>
                <div className="col-1">
                    <h1 className="formsHeaders">Sign Up</h1>
                    <form className='signForm' onSubmit={handleSubmit(onSubmit)}>
                        <span className="headerSpan">Sign up to enjoy the service</span>
                        <p/>
                        <label htmlFor="username">Username:</label>
                        <input type="text" {...register("username",{ required : true, minLength: 3 })} />
                        <p/>
                        <label htmlFor="password">Password:</label>
                        <input type="password" {...register("password",{ required : true, minLength: 8 })} />
                        {errors.username?.type === "required" && "Username is required"}
                        {errors.username?.type === "minLength" && "Username must be longer than 3 characters"}
                        {errors.password?.type === "minLength" && "Password should be longer 8 characters"}
                        <p/>
                        <button className='signButton'>Sign Up</button>
                        <p/>
                        <div>{errorMessage}</div>
                        <p/>
                        <span className="goToRegSpan"> Already have an account?</span>
                        <button className="goToRegButton" onClick = {login}>Sign In</button>
                    </form>
                </div>
            </div>
        </section>
  );
}                    