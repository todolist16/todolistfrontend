import api from "./axios";
import React from "react";
import Moment from "moment";
import ChangeTaskModal  from "./Modals/ChangeTaskModal";
import CreateTaskModal from "./Modals/CreateTaskModal";
import ClearIcon from '@mui/icons-material/Clear';

export default class TasksList extends React.Component {
    constructor(props){
        super(props);
        this.state = {};
        this.state.tasks = [];
        this.updateTask = this.updateTask.bind(this);
        this.updateTaskList = this.updateTaskList.bind(this);
    }
    
    updateTask(task) {
        var current_tasks = this.state.tasks
        for (var index in current_tasks){
            if (current_tasks[index]["id"] === task["id"]){
                current_tasks[index] = task
                break
            }
        }
        this.setState({tasks: current_tasks});
    }

    updateTaskList(task){
        var current_tasks = this.state.tasks;
        current_tasks.push(task);
        this.setState({tasks: current_tasks});
    }

    componentDidMount() {
        api.get("/tasks")
            .then(res => {
                const tasks = res.data;
                for (var tsk in tasks){
                    console.log(res.data[tsk].deadline.toString().substr(0, 16))
                }
                for (var task in tasks){
                    tasks[task]["deadline"] = new Date(tasks[task]["deadline"])
                }
                this.setState({ tasks });
            })
            .catch(err => {
                console.log(err);
            })         
    }

    handleDelete(taskID) {
        api.delete(`/task/${taskID}`)
        .then(res => {
            for (var i in this.state.tasks) {
                if (taskID === this.state.tasks[i]["id"]){
                    this.state.tasks.splice(i, 1);
                }
            }
            this.setState({tasks: this.state.tasks});
        })
        .catch(err => {
            console.log(err,'error');
        });
    }
    
    render() {
        return (
            <div className="mainPage">
                <h1>My Tasks</h1>
                <CreateTaskModal updateTaskList={this.updateTaskList}/>
                <p/>
                <div>
                    <ul>   
                    {this.state.tasks.map((task) => 
                        <ul className="singleTaskList" key={task.name}>
                        <tr>
                            <td className="taskInfo">
                            <ul>
                                <ul>Task: {task.name}</ul>
                                <ul>Desctiprion: {task.description}</ul>
                                <ul>Deadline: {Moment(task.deadline).format("MMMM Do YYYY, h:mm a")}</ul>
                            </ul>
                            </td>
                            <td className="taskBtns">
                            <button className="specialBtns" onClick={() => this.handleDelete(task.id)}> 
                                <ClearIcon />
                            </button>
                            <ChangeTaskModal task={task} updateTask={this.updateTask}/>
                            </td>
                        </tr>
                        </ul>
                    )}
                    </ul>
                </div>
            </div>
        );
    }
}
